import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { isToday, format, parseISO, isAfter, formatDistance } from 'date-fns';
import ptBR from 'date-fns/locale/pt-BR';
import DayPicker, { DayModifiers } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import socketIOClient from "socket.io-client";
import Swal from 'sweetalert2';

import {
  Container,
  Header,
  HeaderContent,
  Profile,
  Content,
  Schedule,
  NextAppointment,
  Section,
  Appointment,
  Calendar,
  Notification,
  NotificationHead,
  NotificationInfo,
  NotificationInfoContent
} from './styles';
import logImg from '../../assets/logo.svg';
import load from '../../assets/load_w.gif';
import { FiBell, FiClock, FiMinusCircle, FiPower, } from 'react-icons/fi';
import { useToast } from '../../hooks/toast';
import { useAuth } from '../../hooks/auth';
import api from '../../service/api';
import { Link } from 'react-router-dom';
import { Constants } from '../../shared/constants';

interface MonthAvailabilityItem {
  day: number,
  available: boolean
}
interface Notification {
  id: string,
  recipient_id: string;
  content: string,
  provider_name: string,
  timeDistance: string;
  read: boolean,
  created_at: string,
}

interface Appointment {
  id: string;
  date: string;
  hourFormatted: string;
  user: {
    name: string;
    avatar_url: string;
  }

}

const Dashboard: React.FC = () => {
  const ENDPOINT = "http://localhost:3333";

  const { signUp, user } = useAuth();
  const { addToast } = useToast();

  const [showNotify, setShowNotify] = useState(false);
  const [showLoading, setShowLoading] = useState(false);
  const [idNotification, setIdNotification] = useState<string>();
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [currentMonth, setCurrentMonth] = useState(new Date());
  const [monthAvailability, setMonthAvailability] = useState<MonthAvailabilityItem[]>([]);
  const [appointments, setAppointments] = useState<Appointment[]>([]);
  const [notification, setNotification] = useState<Notification[]>([]);

  //982501 senha 
  const handleDateChange = useCallback((day: Date, modifiers: DayModifiers) => {
    if (modifiers.available && !modifiers.disabled) {
      setSelectedDate(day)
    }
  }, []);

  const handleMonthChange = useCallback((month: Date) => {
    setCurrentMonth(month)
  }, []);

  const notificationsNotViewed = useMemo(() => {
    const dates = notification
      .filter(
        notify => Object.is(notify.read, false) && Object.is(notify.recipient_id, user.id))
      .map(notify => {
        return notify;
      })
    // const orderBy = dates.sort((a: Notification, b: Notification) => (a.created_at < b.created_at) ? 1 : -1);
    return dates;
  }, [notification, user.id]);


  useEffect(() => {
    const socket = socketIOClient(ENDPOINT);
    socket.on('notification', (notifications: any) => {
      const { notification: notifys } = notifications;

      const notificationFormated: Notification = {
        ...notifys,
        timeDistance: formatDistance(
          parseISO(notifys.created_at),
          new Date(),
          { addSuffix: true, locale: ptBR }
        )
      }
      setNotification([notificationFormated, ...notification]);

      delete notifications.notification;
      const appointmentFormated: Appointment = {
        ...notifications,
        hourFormatted: format(parseISO(notifications.date), 'HH:mm')
      }
      setAppointments([...appointments, appointmentFormated]);
    })
  }, [notification, appointments]);

  useEffect(() => {
    async function loadNotification() {
      const response = await api.get(`/notifications`);

      const data = response.data.map((notification: Notification) => ({
        ...notification,
        timeDistance: formatDistance(
          parseISO(notification.created_at),
          new Date(),
          { addSuffix: true, locale: ptBR }
        )
      }));
      setNotification(data);
    }
    loadNotification();
  }, []);

  useEffect(() => {
    api.get(`/providers/${user.id}/month-availability`, {
      params: {
        year: currentMonth.getFullYear(),
        month: currentMonth.getMonth() + 1,
      }
    }).then(response => {
      setMonthAvailability(response.data)
    })
  }, [currentMonth, user.id]);

  useEffect(() => {
    api.get<Appointment[]>(`/appointments/me`, {
      params: {
        year: selectedDate.getFullYear(),
        month: selectedDate.getMonth() + 1,
        day: selectedDate.getDate(),
      }
    }).then(response => {
      const appointmentsFormatted = response.data.map(appointment => {
        return {
          ...appointment,
          hourFormatted: format(parseISO(appointment.date), 'HH:mm')
        }
      });
      setAppointments(appointmentsFormatted);
    })
  }, [selectedDate]);
  
  const handleCancelAppointment = useCallback(async (appointment: Appointment) => {
    try {
      const options: any = {
        ...Constants.confirm_swal_options,
        text: `Deseja realmente cancelar esse agendamento`,
        confirmButtonText: "Sim",
        title: "Cancelar"
      };
      const { value } = await Swal.fire(options);
      if (value) {
        console.log(value);
        await api.delete(`/delete/${appointment.id}`);
        addToast({
          type: 'success',
          title: 'Agendamento cancelar com sucesso!',
          description: 'O Horário cancelado ficara disponivel para possiveis agendamento!'
        });
          appointments.splice(appointments.indexOf(appointment), 1);
          setAppointments([...appointments]);
      }
    } catch (error) {
      addToast({
        type: 'error',
        title: 'Erro ao cancelar agendamento!',
        description: 'Ocorreu um erro ao cancelar agendamento, Tente novamente.'
      });
    }
  }, [addToast, appointments]);

  const handleUpdatelNotification = useCallback(async (notify: Notification) => {
    try {
        const {id} = notify;
        setIdNotification(id);
        setShowLoading(true);
        await api.put(`notifications/update`, {
          id,
          read: true
        });
        addToast({
          type: 'success',
          title: 'Notificação atualizada com sucesso!',
          description: 'Notificação foi marcada como lida!'
        });
        notification.splice(notification.indexOf(notify), 1);
        setNotification([...notification]);
        setShowLoading(false);
    } catch (error) {
      addToast({
        type: 'error',
        title: 'Erro ao atualizada notificação!',
        description: 'Ocorreu um erro ao atualizada notificação, Tente novamente.'
      });
    }
  }, [notification, addToast]);

  const morningAppointments = useMemo(() => {
    return appointments.filter(appointment => {
      return parseISO(appointment.date).getHours() < 12;
    })
  }, [appointments]);

  const afternoonAppointments = useMemo(() => {
    return appointments.filter(appointment => {
      return parseISO(appointment.date).getHours() >= 12;
    })
  }, [appointments]);
 


  const disabledDays = useMemo(() => {
    const dates = monthAvailability
      .filter(monthDay => monthDay.available === false)
      .map(monthDay => {
        const year = currentMonth.getFullYear();
        const month = currentMonth.getMonth();

        return new Date(year, month, monthDay.day)
      })
    return dates
  }, [currentMonth, monthAvailability]);

  const selectedDateAsText = useMemo(() => {
    return format(selectedDate, "'Dia' dd 'de' MMMM", {
      locale: ptBR,
    })
  }, [selectedDate]);

  const selectedWeekDay = useMemo(() => {
    return format(selectedDate, 'cccc', {
      locale: ptBR,
    })
  }, [selectedDate]);

  const nextAppointment = useMemo(() => {
    return appointments.find(appointment => {
      return isAfter(parseISO(appointment.date), new Date())
    })
  }, [appointments])

  const notifyButton = useCallback(() => {
    setShowNotify((state) => !state);
  }, []);

  return (
    <Container>
      <Header>
        <HeaderContent>
          <img src={logImg} alt="GoBarber" />
          <Profile>
            <img src={user.avatar_url} alt={user.name} />
            <div>
              <span>Bem-vindo</span>
              <Link to="/profile">
                <strong>{user.name}</strong>
              </Link>
            </div>

          </Profile>
          <Notification>
            <button onClick={notifyButton}>
              <FiBell size={25} />
              {notificationsNotViewed.length > 0 && (
                <sup >{notificationsNotViewed?.length}</sup>
              )}
            </button>
            {showNotify && (
              <div>
                <NotificationHead>Notificações</NotificationHead>
                <NotificationInfo>
                  {notificationsNotViewed.map(notify => (
                    <NotificationInfoContent key={notify.id} 
                    onClick={() => handleUpdatelNotification(notify)}>
                      <strong>{notify.provider_name}</strong>
                      <p>{notify.content}</p>
                      <div>
                        <span>{notify.timeDistance}<sup></sup></span>
                        {showLoading && idNotification === notify.id && (
                        <img src={load} alt="load"/>
                        )}
                      </div>
                    </NotificationInfoContent>
                  ))}
                  {notificationsNotViewed.length === 0 && (
                    <p>Ainda não tem nada por aqui</p>
                  )}
                </NotificationInfo>
              </div>
            )}

          </Notification>
          <button type="button" onClick={signUp}>
            <FiPower />
          </button>
        </HeaderContent>
      </Header>

      <Content>
        <Schedule>
          <h1>Horários agendados</h1>
          <p>
            {isToday(selectedDate) && <span>Hoje</span>}
            <span>{selectedDateAsText}</span>
            <span>{selectedWeekDay}</span>
          </p>

          {isToday(selectedDate) && nextAppointment && (
            <NextAppointment>
              <strong>Agendamentos a seguir</strong>
              <div>
                <img src={nextAppointment.user.avatar_url} alt={user.name}
                />
                <strong>{nextAppointment.user.name}</strong>
                <span>
                  <FiClock />
                  {nextAppointment.hourFormatted}
                </span>
              </div>
            </NextAppointment>)}
          <Section>
            <strong>Manha</strong>

            {morningAppointments.length === 0 && (
              <p>Nenhum agendamento neste período</p>
            )}

            {morningAppointments.map(appointment => (
              <Appointment key={appointment.id}>
                <span>
                  <FiClock />
                  {appointment.hourFormatted}
                </span>
                <div>
                  <img src={appointment.user.avatar_url} alt={user.name}
                  />
                  <strong>{appointment.user.name}</strong>
                  <button onClick={() => handleCancelAppointment(appointment)}><FiMinusCircle /></button>
                </div>
              </Appointment>
            ))}

          </Section>
          <Section>
            <strong>Tarde</strong>
            {afternoonAppointments.length === 0 && (
              <p>Nenhum agendamento neste período</p>
            )}

            {afternoonAppointments.map(appointment => (
              <Appointment key={appointment.id}>
                <span>
                  <FiClock />
                  {appointment.hourFormatted}
                </span>
                <div>
                  <img src={appointment.user.avatar_url} alt={user.name}
                  />
                  <strong>{appointment.user.name}</strong>
                  <button onClick={() => handleCancelAppointment(appointment)}><FiMinusCircle /></button>
                </div>
              </Appointment>
            ))}
          </Section>
        </Schedule>
        <Calendar>
          <DayPicker
            weekdaysShort={['D', 'S', 'T', 'Q', 'Q', 'S', 'S']}
            fromMonth={new Date()}
            disabledDays={[{ daysOfWeek: [0, 6] }, ...disabledDays]}
            modifiers={{
              available: { daysOfWeek: [1, 2, 3, 4, 5] },
            }}
            selectedDays={selectedDate}
            onDayClick={handleDateChange}
            onMonthChange={handleMonthChange}
            months={[
              'Janeiro',
              'Fevereiro',
              'Março',
              'Abril',
              'Maio',
              'Junho',
              'Julho',
              'Agosto',
              'Setembro',
              'Outubro',
              'Novembro',
              'Dezembro']}
          />
        </Calendar>
      </Content>
    </Container>
  );
}

export default Dashboard;