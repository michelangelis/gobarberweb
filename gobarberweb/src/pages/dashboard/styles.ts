import styled from 'styled-components';
import { shade } from 'polished';
import ArrowLeftIcon from '../../assets/arrowLeft.png';
import ArrowRightIcon from '../../assets/arrowRight.png';
export const Container = styled.div`
  color: #FFF;
`;

export const Header = styled.header`
 padding: 32px 0;
 background: #28262e;
`;

export const Notification = styled.div`
 position: relative;
 display: flex;
 justify-content: center;
 margin-left: 60px;
 
 button{
   position: relative;
    sup{
      position: absolute;
      top: -10px;
      left: 10px;
      width: 20px;
      height: 20px;
      border-radius: 50%;
      background: #ff9000;
      color: #fff;
    }
  }

 > div{
  position: absolute;
  left: 0px;
  top: calc(100% + 25px);
  width: 448px;
  box-shadow: rgb(0 0 0 / 60%) 0px 5px 20px;
  display: block;
  z-index: 102;
  border-radius: 5px;
  &::before {
    content: "";
    position: absolute;
    top: calc(-11% + 17px);
    left: 3%;
    transform: translateX(-50%);
    width: 0px;
    height: 0px;
    border-style: solid;
    border-width: 0px 7.5px 8px;
    border-color: transparent transparent rgb(41, 41, 46);
    z-index: 103;
  }
  
 }
`;

export const ButtonBell = styled.button`

`;
export const NotificationHead = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: rgb(41, 41, 46);
  height: 50px;
  padding: 0px 20px;
  border-radius: 5px 5px 0px 0px;
  
`;
export const NotificationInfo = styled.div`
  display: flex;
  flex-direction: column;
  background: rgb(32, 32, 36);
  justify-content: space-between;
  height: calc(160px);
  width: 100%;
  border-radius: 0px 0px 5px 5px;
  font-size: 14px;
  color: rgb(168, 168, 179);
  padding: 5px 20px;
  overflow-y: scroll;
`;
export const NotificationInfoContent = styled.div`
  padding: 10px 0;
  border-bottom: 1px dashed #3e3b47;
  cursor: pointer;
  strong{
    color: #f4ede8;
  }
  p{
    color:#ff9000;
  }
  div{
    display: flex;
    justify-content: space-between;
    span{
    font-size: 11px;
    position: relative;
    sup{
      position: absolute;
      top: 5px;
      right: -11px;
      width: 7px;
      height: 7px;
      border-radius: 50%;
      background: #ec0c33;
      /* background: #00ff8c; */
      color: #fff;
    }
   }
   img{
     width: 20px;
     height: 20px;
     position: relative;
     top: -17px;
   }
  }
  
  &:last-of-type{
    border-bottom: 0 !important;
   }
`;

export const HeaderContent = styled.div`
  max-width: 1120px; 
  margin: 0 auto;
  display: flex;
  align-items: center;
  > img {
    height: 80px;
  }
  button{
    margin-left: auto;
    background: transparent;
    border: none;
    svg{
      color: #999591;
      width: 20px;
      height: 20px;
    }
  }
  @media(max-width: 900px) {
    margin: 0 60px;
 }
`;
export const Profile = styled.div`
  display: flex;
  align-items: center;
  margin-left: 80px;

  img{
    width: 56px;
    height: 56px;
    border-radius: 50%;
  }

  > div{
    display: flex;
    flex-direction: column;
    margin-left: 16px;
    line-height: 24px;

    span{
      color: #f4ede8;
    }
    a{
      text-decoration: none;
      color: #ff9000;
      &:hover{
        opacity: 0.8;
      }
    }
  }
  @media(max-width: 900px) {
    margin-left: 40px;
  }
`;

export const Content = styled.main`
  max-width: 1120px;
  margin: 64px auto;
  display: flex;

  @media(max-width: 900px) {
    flex-direction: column-reverse;
    position: relative;
  }

`;
export const Schedule = styled.div`
  flex: 1;
  margin-right: 120px;

  h1{
    font-size: 36px;
  }
  p{
    margin-top:8px;
    color: #ff9000;
    display: flex;
    align-items: center;
    font-weight: 500;

    span{
      display: flex;
      align-items: center;
    }
    span + span::before{
      content: '';
      width: 1px;
      height: 12px;
      background: #ff9000;
      margin: 0 8px;
      
    }
  }
  @media(max-width: 900px) {
    margin: 0 60px;
  }
`;
export const NextAppointment = styled.div`
    margin-top: 64px;

    > strong{
      color: #999591;
      font-size: 20px;
      font-weight: 400;
    }
    div{
      background: #3e3b47;
      display: flex;
      align-items: center;
      padding: 16px 24px;
      border-radius: 10px;
      margin-top: 24px;
      position: relative;

      &::before{
        position: absolute;
        height: 80%;
        width: 1px;
        left:0;
        top: 10%;
        content:'';
        background: #ff9000;
      }
      img{
        height: 80px;
        width: 80px;
        border-radius: 50%;
      }

      strong{
        margin-left: 24px;
        color: #fff;
      }
      span{
        margin-left: auto;
        display: flex;
        align-items: center;
        color: #999591;

        svg{
          color: #ff9000;
          margin-right: 8px;
        }
      }
    }
`;
export const Section = styled.section`
  margin-top: 48px;

  > strong{
    color: #999591;
    font-size: 20px;
    line-height: 26px;
    border-bottom: 1px solid #3e3b47;
    display: block;
    padding-bottom: 16px;
    margin-bottom: 16px;
  }
  > p{
    color: #999591;
  }
`;
export const Appointment = styled.div`
    display: flex;
    align-items: center;

    & + div{
      margin-top: 16px;
    }

    span{
        margin-left: auto;
        display: flex;
        align-items: center;
        color: #f4ede8;
        width: 70px;

        svg{
          color: #ff9000;
          margin-right: 8px;
        }
      }
   div{
      flex: 1;
      background: #3e3b47;
      display: flex;
      align-items: center;
      padding: 16px 24px;
      border-radius: 10px;
      margin-left: 24px;

      img{
        height: 56px;
        width: 56px;
        border-radius: 50%;
      }

      strong{
        margin-left: 24px;
        color: #fff;
        font-size: 20px;
      }
      button{
        margin-left: auto;
        background: transparent;
        border: none;
        color: #ff1e1e;
        font-size: 17px;
      }
   }
`;
export const Calendar = styled.aside`
  width: 380px;
  .DayPicker {
  border-radius: 10px;

  &-wrapper {
    padding-bottom: 0;
    background: #28262e;
    border-radius: 10px;
    z-index: 0;
  }

  &-NavBar {
    position: relative;

    ::before {
      content: '';
      width: 100%;
      height: 50px;
      position: absolute;
      background: #3e3b47;
      border-radius: 10px 10px 0 0;
      z-index: -1;
    }
  }

  &-NavButton {
    color: #999591 !important;
    margin-top: 0;
    top: 0;

    &--prev {
      background: url(${ArrowLeftIcon}) no-repeat center;
      margin-right: 0;
      left: 12px;
      width: 50px;
      height: 50px;
      background-size: 25px;
    }

    &--next {
      background: url(${ArrowRightIcon}) no-repeat center;
      right: 12px;
      width: 50px;
      height: 50px;
      background-size: 25px;
    }
  }

  &-Month {
    border-collapse: separate;
    border-spacing: 8px;
    margin: 0;
    padding: 0 10px 10px;
  }

  &-Caption {
    line-height: 50px;
    color: #f4ede8;

    > div {
      text-align: center;
    }
  }

  &-Weekday {
    color: #666360;
    font-size: 16px;
  }

  &-Day {
    width: 40px;
    height: 40px;
    transition: all 0.2s ease;
    border-radius: 10px;

    &--today {
      font-weight: normal;
      color: #fff;
    }

    &--available:not(.DayPicker-Day--outside) {
      background: #3e3b47;
      border-radius: 10px;
    }

    &--disabled {
      color: #666360;
      background: transparent !important;
    }

    &--selected:not(.DayPicker-Day--disabled) {
      background: #ff9000 !important;
      color: #232129 !important;
    }
  }

  &:not(.DayPicker--interactionDisabled)
    .DayPicker-Day:not(.DayPicker-Day--disabled):not(.DayPicker-Day--selected):not(.DayPicker-Day--outside):hover {
    background: ${shade(0.2, '#3e3b47')};
  }
}
@media(max-width: 900px) {
    margin-left: 60px;
    margin-bottom: 20px;
    position: relative;
}
`;
